# mysql issue


## 1. socket的位置

> 连接mysqld时出现 `/tmp/mysql.sock` 的有关问题

配置文件的路径：`/etc/my.cnf`

```bash
vi /etc/my.cnf
```

查看其中socket的值

然后通过指定socket参数来连接mysqld：

```bash
mysql -u root -p --socket=/var/lib/mysql/mysql.sock
```


## 2. 数据目录权限

正确配置mysql数据目录和套接字文件的权限


```bash
sudo chown -R mysql:mysql /var/lib/mysql
sudo chmod 755 /tmp/mysql.sock
```



## 3. 检查端口和实例


```bash
sudo netstat -tuln | grep 3306
```

```bash
ps aux | grep mysqld
```


## 4. 查看错误日志文件

```bash
vi /var/log/mysql/error.log
```


## 5. 重新初始化mysqld

```bash
systemctl stop mysqld
rm -rf /var/lib/mysql/
cat /var/log/mysqld.log | grep password
chown -R mysql:mysql /var/lib/mysql
systemctl start mysqld
mysql -u root -p
```


## 6. 密码策略

```bash
vi /etc/my.cnf
```

设置：

```conf
plugin-load-add=validate_password.so
validate-password=FORCE_PLUS_PERMANENT
```

重启服务：

```bash
systemctl restart mysqld
```

```sql
show variables like 'validate%';
set global validate_password_policy=0;  #修改密码安全策略为低 
set global validate_password_length=6;
ALTER USER 'root'@'localhost' IDENTIFIED BY '123456';
```

## 7. 远程管理权限

```sql
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '123456';
```



## 8. 导入数据

使用-D指定数据库为test

```bash
mysql -uroot -p -Dtest<test.sql
```